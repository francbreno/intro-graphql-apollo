import mongoose from "mongoose";

import AuthorModel from "./models/author";

const resolvers = {
  Query: {
    authors() {
      return AuthorModel.find();
    },
    author(root, { id }) {
      return AuthorModel.findOneAndUpdate({ id });
    }
  },
  Mutation: {
    addAuthor(root, args) {
      const author = new AuthorModel(args);
      return author.save();
    },
    deleteAuthor(root, { id }) {
      return AuthorModel.findOneAndRemove({ id });
    },
    updateAuthor(root, { id, name }) {
      return AuthorModel.findOneAndUpdate({ id }, { name });
    }
  }
};

export default resolvers;
