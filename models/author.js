import mongoose from 'mongoose';
import uuid from 'node-uuid';

const Schema = mongoose.Schema;

const AuthorSchema = new Schema({
  id: { type: String, default: uuid.v4 },
  name: String,
  age: Number,
  books: [String]
});

const model = mongoose.model('author', AuthorSchema);
export default model;
